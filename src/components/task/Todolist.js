import React, { Component } from "react";
import NewTask from "../task/NewTask";
import  Doing  from "../task/Doing";
class Todolist extends Component {
 
  render() {
    
    const { items, EditTask, DeleteTask, handleInprogress,handleModal} = this.props;

    return (
        <div>
           <div className="card">
            <div className="card-body">
              <h4 className="card-title" id="task">
                 ALL Task
            </h4>
          </div>
        </div>
        <div className="card-group">
          <div className="card" style={{ width: "20rem" }}>
            <div className="card-body1">
              <h5 className="card-title1">NewTask</h5>
              <ul onChange =  {handleInprogress} id="list_item">
                {
                   console.log(items),
                   console.log(items.id),
                 
                    <div className="flex-item item">
                      {
                        
                        items.map((item) => { 
                            
                         return (
                          <NewTask
                                
                             key={item.id}
                             title={item.title}
                             EditTask = {() => EditTask(item.id)}
                             DeleteTask = {() => DeleteTask(item.id)}
                             handleModal = {() => handleModal()}
                             handleInprogress = {() => handleInprogress()}

                       />   
                        );
                      })
                    }
                </div>
               }
  
              </ul>
            </div>
          </div>
     
          <div className="card" style={{ width: "20rem" }}>
            <div className="card-body2">
              <h5 className="card-title2">InProgress</h5>
              <ul className="Inprogress" id="list_item1">
                {
                   
                <div className="flex-item item">
                   {/* {
                        items1.map((item) => {
                            
                          return (
                           <Doing
                                
                              key={item.id}
                              title={item.title}
                              handleEdit = {() => handleEdit(item.id)}
                              handleDelete = {() => handleDelete(item.id)}
                              
                        />
                         );
                       })
                   
                   }        */}
                </div>
               } 
              </ul>
            </div>
          </div>
               
          <div className="card" style={{ width: "20rem" }}>
            <div className="card-body3">
              <h5 className="card-title3">Done</h5>
              <ul className="Done" id="list_item2">
                <div className="flex-item item"></div>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Todolist;
