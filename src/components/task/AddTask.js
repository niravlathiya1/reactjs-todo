import React, {Component} from 'react';
import {Button,
    Modal
  } from 'reactstrap';

class MyModal extends Component{

    constructor(props){
        
        super(props);
       
    } 


    render()
    {
        const {item,isshow, handleChange,handleSubmit,handleModal,closeModal} = this.props;

     return(
         
      <div >   
             <button className="btn btn-primary" type="button" data-toggle="modal" data-target = "#exampleModal" onClick = {handleModal.bind(this)} >
                  Add Task </button> 
       
            <Modal isOpen={isshow}>
            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" area-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">
                      Add Task Here</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick ={closeModal.bind(this)}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div> 

              <form onSubmit={handleSubmit} >
                  <div className="modal-body">
                    <div className="form-group"/>
                        <label htmlFor="title" className="todo-input">Enter TaskName</label>
                       
                         <input 
                           type="text"
                           className="item_input form-control" 
                           name="title1" 
                           value = {item}
                           onChange = {handleChange}
                           placeholder="enter here"   
                         
                         required/>

                  <div className="modal-footer"/>

                    <button className="btn btn-primary" data-dismiss="modal" onClick ={closeModal.bind(this)}>Close</button>
                      
                    <button 
                       type = "submit"
                       className="btn btn-primary"
            
                    > Add Task</button>

                  </div>
                </form>
              </div>
            </div>
        </div>
        </Modal>
          </div>
           
        );
    }
}

export default MyModal;