import React, { Component } from 'react';
import Moment from 'react-moment';
import{Modal} from 'reactstrap';
import { Edit } from "./EditTask";
class NewTask extends Component {

   constructor(props){
       super(props);
   }
  
   
render(){
        const dateToFormat = '1976-04-19T12:59-0500';
        const {id,handleInprogress,title, DeleteTask,handleModal} = this.props;
        return(

          <div>
            
              <span><li>{title}</li><i className="fa fa-pencil-alt" data-toggle="modal" data-target = "#exampleModal1"style={{paddingleft:"70px"}} onClick = {handleModal}></i>
                 <i className="fas fa-trash-alt" onClick = {() => DeleteTask(id)} style={{paddingleft: "25px"}} ></i>
                 <Moment><p>{dateToFormat}</p></Moment>
              </span>
              
             <button onClick={() => handleInprogress(id)} className={`btn btn-primary`}>inprogress</button>
          
         </div>
          
   
        )
    }
}

export default NewTask;