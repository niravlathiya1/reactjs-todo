import React, {Component} from "react";
import {Modal} from "reactstrap";
import NewTask from "./NewTask";
import { BrowserRouter as Router } from "react-router-dom";

class Edit extends Component {

    constructor(props){

        super(props);
        
        // this.Open = this.Open.bind(this);
        // this.Close = this.Close.bind(this);
    }

  //  Open = () => {
  //      console.log("open modal");
  //       this.setState({
  //           isshow: true
  //       })
       
  //  }

  //  Close = () => {
  //      this.setState({
  //           isshow: false
  //      })
  //  }

   render = () => {
     
       const {EditTask,item,handleChange,closeModal,isshow} = this.props;
    return(
     <Router>  
        <div isopen={isshow}>
        <div className="modal fade" id="exampleModal1" tabIndex="-1" aria-labelledby="exampleModalLabel" area-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                  Add Task Here</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick = {closeModal}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div> 
           
          <form onSubmit = {EditTask}>
              <div className="modal-body">
                <div className="form-group"/>
                    <label htmlFor="title" className="todo-input">Enter TaskName</label>
                   
                     <input 
                       type="text"
                       className="item_input form-control" 
                       name="title1" 
                       value = {item}
                       onChange = {handleChange}
                       placeholder="enter here"   
                     
                     required/>
                 
              <div className="modal-footer"/>
                
                <button className="btn btn-primary" data-dismiss="modal" onClick ={closeModal}>Close</button>

                <button 
                   type = "submit"
                   className="btn btn-primary"
                 > Edit Task</button>

              </div>
            </form>
          </div>
        </div>
     </div>
  </div>
   
 </Router>   

    )
   }
}

export default Edit;