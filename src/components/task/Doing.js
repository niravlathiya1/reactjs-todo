import React, {Component} from 'react';
import Moment from 'react-moment';
class Doing extends Component {


    render() {
    
        const dateToFormat = '1976-04-19T12:59-0500';
        const {title,updateStatus,handleDelete,handleEdit,id} = this.props;
        return(
            <div>
              <span><li>{title}</li><i className="fa fa-pencil-alt" onClick = {() => handleEdit(id)} style={{paddingleft:"70px"}}></i>
                 <i className="fas fa-trash-alt" onClick = {() => handleDelete(id)} style={{paddingleft: "25px"}}></i> 
                <Moment><p>{dateToFormat}</p></Moment>
             </span>

              <button onClick={() => updateStatus("done")} className="btn btn-primary">Done</button>
           
           </div>
        )
    }
}

export default Doing;