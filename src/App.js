import logo from "./logo.svg";
import "./App.css";
import React, { Component } from "react";
import { v4 as uuid4 } from "uuid";
import { BrowserRouter as Router } from "react-router-dom";
import MyModal from "./components/task/AddTask";
import Todolist from "./components/task/Todolist";
import Edit from "./components/task/EditTask"


class App extends Component {
  constructor(props) {
    super(props); 
  
    this.state = {
      items : [],
      items1: [],
      item : '',
      id   : uuid4(),
      isshow: false
      
    };

  
  }
  handleModal = () => {

    this.setState({
        isshow: true
    })
}

 closeModal = () => {
    this.setState({
        isshow:false
    })
}


  handleChange = (e) => {
    this.setState({
      item: e.target.value,
    });
  };

  handleSubmit = (e) => {
    try{
         e.preventDefault();

    const newitem = {
      id: this.state.id,
      title: this.state.item,
      
    };

    const updateitems = [...this.state.items, newitem];

    if(this.state.item.length > 0){
      this.setState({
         items: updateitems,
         id: uuid4(),
         item: "",
         isEdit:false
      
      });
      console.log(this.state.items);
    }
     
  }catch(err){
      
        console.log("page reload",err);
   }
  };

  handleInprogress = () => {
 

    const itemprogress = {
      id: this.state.id,
      title: this.state.item,
      
    };
    console.log("progress",itemprogress);
    const doingitems = [...this.state.items1, itemprogress];
  
    if(this.state.item.length > 0){
      this.setState({
         items1: doingitems,
         id: uuid4(),
         item: "",
         
      });
      console.log("items",this.setState.items1);
    }
  }

  EditTask = (id) => {
       
        const selectitems = this.state.items.find(item => item.id === id)
        const filterItem = this.state.items.filter(item => item.id !== id)
           
        console.log("edit here filteritems");
        this.setState({

             items: filterItem,
             id: id,
             item: selectitems.title,
             isEdit: true
        })
  }

  DeleteTask = (id) => {

     const filteritems = this.state.items.filter(item => item.id !== id);
     console.log("delete id",id);
     this.setState({

         items: filteritems
     })
}


  render() {
     
    return (

      <Router>
        <MyModal
          item = {this.state.item}
          isshow = {this.state.isshow}
          handleChange = {this.handleChange}
          handleSubmit = {this.handleSubmit}
          handleModal = {this.handleModal}
          closeModal = {this.closeModal}
        />

        <Edit
          item = {this.state.item}
          handleChange = {this.handleChange} 
          closeModal = {this.closeModal}
         
        />
        <Todolist 
             
           items={this.state.items} 
           isshow = {this.state.is}
           handleInprogress = {this.handleInprogress}
           DeleteTask = {this.DeleteTask}
           EditTask = {this.EditTask}
           handleModal = {this.handleModal}

        />
      </Router>
    );
  }
}

export default App;
